#!/bin/sh

echo "Building docker image"
echo "VERSION = ${VERSION}"

# Change to top directory
cd ../"${0%/*}"

docker build -t registry.gitlab.com/c5219/crypto-tracker-frontend:$VERSION -f ./docker/Dockerfile .
docker push registry.gitlab.com/c5219/crypto-tracker-frontend:$VERSION
