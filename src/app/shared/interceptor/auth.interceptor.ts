import {Injectable} from '@angular/core';
import {
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
    HttpResponse
} from '@angular/common/http';
import {SessionService} from '../service/session.service';
import {Observable} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {MessageService} from 'primeng/api';
import {TokenService} from "../../codegen";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(
        private sessionService: SessionService,
        private messageService: MessageService,
        private tokenService: TokenService
    ) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        // Ignore requests not directed to backend
        if (!req.url?.startsWith(environment.apiUrl)) {
            return next.handle(req);
        }

        // Check if token is expired, if so, logout
        if (this.sessionService.authenticated === false) {
            this.sessionService.logout();
        }

        // If we have a session and we are authenticated, add token to the headers
        if (this.sessionService.authenticated && this.sessionService.jwt) {
            req = req.clone({
                headers: req.headers.set('Authorization', 'Bearer ' + this.sessionService.jwt)
            });
        }

        // @ts-ignore
        return next.handle(req)
            .pipe(
                map((response: HttpResponse<any>) => {
                    return response;
                }),
                catchError((error, caught) => {

                        // User is doing some nasty stuff, log him out
                        if (error.status === 403) {
                            this.sessionService.logout();
                            return next.handle(req);
                        }

                        //Token expired (refresh)
                        if (error.status === 401 && this.sessionService.jwt_refresh) {
                            this.tokenService.postRefreshTokenItem({
                                refresh_token: this.sessionService.jwt_refresh
                            }).subscribe((token) => {
                                this.sessionService.jwt = token.token;
                                this.sessionService.jwt_refresh = token.refresh_token;

                                // Retry
                                req = req.clone({
                                    setHeaders: {
                                        'Authorization': 'Bearer ' + this.sessionService.jwt
                                    }
                                });

                                return next.handle(req);
                            });
                        } else {
                            this.throwErrorMessage(error);
                        }
                        return next.handle(req);
                    }
                ));
    }

    private throwErrorMessage(error) {
        console.log(error)
        if (error.error) {
            const errorMessage = error.message;
            const message = error.error.detail && error.error.detail !== errorMessage ?
                {
                    severity: 'error',
                    summary: error.error.title,
                    detail: error.error.detail
                } :
                {
                    severity: 'error',
                    summary: errorMessage,
                }
            this.messageService.add(message);
        } else {
            this.messageService.add({
                severity: 'error',
                summary: error.status + '',
                detail: error.message
            });
        }
    }

}
