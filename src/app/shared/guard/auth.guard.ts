import {Injectable} from '@angular/core';
import {
    ActivatedRouteSnapshot,
    CanActivate,
    CanLoad,
    Route,
    Router,
    RouterStateSnapshot,
    UrlSegment
} from '@angular/router';
import {SessionService} from '../service/session.service';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad {

    constructor(
        private sessionService: SessionService,
        private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.confirmAuthElseRedirect();
    }

    canLoad(route: Route, segments: UrlSegment[]): boolean | Observable<boolean> | Promise<boolean> {
        return this.confirmAuthElseRedirect();
    }

    private confirmAuthElseRedirect() {
        if (this.sessionService.authenticated) {
            return true;
        } else {
            this.router.navigate(['/login']);
            return false;
        }
    }
}
