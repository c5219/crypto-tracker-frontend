import {Injectable} from '@angular/core';
import jwt_decode from 'jwt-decode';
import {Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class SessionService {

    authenticated: boolean;
    username: string;
    expiryDate: Date;
    scope: Array<string>;

    constructor(
        private router: Router
    ) {
        this.updateProperties(this.jwt);
    }

    public get jwt() {
        return localStorage.getItem('jwt');
    }

    public set jwt(jwt: string) {
        localStorage.setItem('jwt', jwt);
        this.updateProperties(jwt);
    }

    public get jwt_refresh() {
        return localStorage.getItem('jwt_refresh')
    }

    public set jwt_refresh(token: string) {
        localStorage.setItem('jwt_refresh', token);
    }

    private updateProperties(jwt: string) {

        let decoded;

        if (jwt != null && jwt.length > 4) {
            decoded = jwt_decode(jwt);
        }

        if (decoded === undefined || decoded.exp === undefined) {
            this.authenticated = false;
            this.expiryDate = null;
            this.scope = null;
            this.username = null;
        } else {
            this.username = decoded.sub;
            this.scope = decoded.scope;
            this.expiryDate = new Date(0);
            this.expiryDate.setUTCSeconds(decoded.exp);
            this.authenticated = this.expiryDate.valueOf() > new Date().valueOf();
        }
    }

    public logout() {
        this.jwt = null;
        this.jwt_refresh = null;
        localStorage.removeItem('jwt');
        localStorage.removeItem('jwt_refresh');
        this.router.navigateByUrl('/login');
    }
}
