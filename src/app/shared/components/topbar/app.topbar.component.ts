import {Component, OnDestroy, OnInit} from '@angular/core';
import {AppComponent} from '../../../app.component';
import {AppMainComponent} from '../../../app.main.component';
import {Subscription} from 'rxjs';
import {SessionService} from "../../service/session.service";

@Component({
    selector: 'app-topbar',
    templateUrl: './app.topbar.component.html',
    styleUrls: ['./app.topbar.component.scss']
})
export class AppTopBarComponent implements OnInit, OnDestroy {

    subscription: Subscription;

    constructor(public app: AppComponent,
                public appMain: AppMainComponent,
                public sessionService: SessionService
    ) {
    }

    ngOnInit(): void {
    }

    logout() {
        this.sessionService.logout();
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
}
