import {Component, OnInit} from '@angular/core';
import {AppComponent} from '../../app.component';
import {FormBuilder, Validators} from '@angular/forms';
import {SessionService} from '../../shared/service/session.service';
import {Router} from '@angular/router';
import {TokenService, UserService} from "../../codegen";
import {MessageService} from "primeng/api";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    loginForm = this.fb.group({
        username: ['', Validators.required],
        password: ['', Validators.required],
        remember: [false]
    });

    constructor(
        public app: AppComponent,
        private fb: FormBuilder,
        private tokenService: TokenService,
        private userService: UserService,
        private sessionService: SessionService,
        private messageService: MessageService,
        private router: Router
    ) {
    }

    ngOnInit(): void {
    }

    onSubmit() {
        this.tokenService.postCredentialsItem({
            username: this.loginForm.value.username,
            password: this.loginForm.value.password
        }).subscribe((r) => {
            if (r) {
                this.sessionService.jwt = r.token;
                this.sessionService.jwt_refresh = r.refresh_token
                this.router.navigateByUrl('/');
            }
        });
    }

    register() {
        this.userService.postUserCollection({
            username: this.loginForm.value.username,
            password: this.loginForm.value.password
        }).subscribe((r) => {
            this.messageService.add({
                severity: 'success',
                summary: 'Successfully registered'
            })
            this.onSubmit();
        });
    }
}
