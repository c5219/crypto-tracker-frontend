import {Component, OnInit} from '@angular/core';
import {MenuItem} from 'primeng/api';
import {Product} from '../../api/product';
import {WalletService, WalletWalletOutputDTO} from "../../codegen";
import {DashboardChartConfiguration} from "./dashboardChartConfiguration";

@Component({
    templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {

    chartColors = DashboardChartConfiguration.pieChartColors;
    barOptions = DashboardChartConfiguration.barChartOptions;

    items: MenuItem[];

    products: Product[];

    wallets: WalletWalletOutputDTO[] = [];

    investedData: any;
    currentValueData: any;
    pieOptions: any;

    barData: any;

    summary: any;

    constructor(
        private walletService: WalletService
    ) {
        walletService.getWalletCollection(null).subscribe(wallets => {
            this.wallets = wallets;
            this.initSummary();
            this.initCharts();
        });
    }

    ngOnInit() {
    }

    private initSummary() {
        let summary = [];

        this.wallets.forEach(w => {
            let currencySummary = summary.find(s => s.fiatCurrency == w.fiatCurrency)
            if (!currencySummary) {
                currencySummary = {
                    fiatCurrency: w.fiatCurrency,
                    invested: 0,
                    value: 0
                };
                summary.push(currencySummary);
            }

            currencySummary['invested'] += Number(w.totalFiatInvested)
            currencySummary['value'] += Number(w.fiatWalletValue);
        })

        this.summary = summary;
    }

    private initCharts() {
        this.investedData = {
            labels: this.wallets.map(w => w.name),
            datasets: [
                {
                    data: this.wallets.map(w => w.totalFiatInvested),
                    ...this.chartColors
                }
            ]
        };

        this.currentValueData = {
            labels: this.wallets.map(w => w.name),
            datasets: [
                {
                    data: this.wallets.map(w => w.fiatWalletValue),
                    ...this.chartColors
                }
            ]
        }

        this.pieOptions = {
            plugins: {
                legend: {
                    labels: {
                        fontColor: '#A0A7B5'
                    }
                }
            }
        };

        this.barData = {
            labels: this.wallets.map(w => w.name),
            datasets: [
                {
                    label: 'Coin return on investment',
                    backgroundColor: '#90cd93',
                    data: this.wallets
                        .map(w => parseFloat(w.totalFiatInvested) == 0 ? "0" : w.roiPercent)
                }
            ]
        };
    }
}
