import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Transaction, TransactionService} from "../../../codegen";
import {MessageService} from "primeng/api";
import {EventService} from "../../../shared/service/event.service";
import {EventEnum} from "../../../shared/service/model/event-enum";

@Component({
    selector: 'app-create-transaction',
    templateUrl: './create-transaction.component.html',
    styleUrls: ['./create-transaction.component.scss']
})
export class CreateTransactionComponent implements OnInit {

    @Input()
    walletId: string;

    @Input()
    currency: string;

    @Output()
    onClose = new EventEmitter<void>();

    form: FormGroup;

    constructor(
        private fb: FormBuilder,
        private transactionService: TransactionService,
        private messageService: MessageService,
        private eventService: EventService
    ) {

        this.form = this.fb.group({
                datetime: [new Date(), [Validators.required]],
                coinValue: [undefined, Validators.required],
                coinAmount: [undefined, Validators.required],
                fiatInvested: [undefined, Validators.required],
                keepInserting: [false]
            }
        )
    }

    ngOnInit(): void {
    }

    submit() {
        const transaction: Transaction = {
            wallet: '/api/wallets/' + this.walletId,
            datetime: this.form.value['datetime'],
            coinValue: this.form.value['coinValue'].toString(),
            coinAmount: this.form.value['coinAmount'].toString(),
            fiatInvested: this.form.value['fiatInvested'].toString()
        }
        this.transactionService.postTransactionCollection(transaction)
            .subscribe({
                next: (transaction) => {
                    this.messageService.add({
                        severity: 'success',
                        summary: 'Transaction created'
                    });

                    this.eventService.emitEvent({
                        type: EventEnum.TRANSACTION_CREATED,
                        value: transaction
                    });

                    if (this.form.value["keepInserting"]) {
                        this.form.reset();
                    } else {
                        this.onClose.emit();
                    }
                }
            });
    }
}
