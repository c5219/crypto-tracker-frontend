import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TransactionService, TransactionTransactionOutputDTO} from "../../../codegen";
import {ConfirmationService, ConfirmEventType, MessageService} from "primeng/api";
import {EventService} from "../../../shared/service/event.service";
import {EventEnum} from "../../../shared/service/model/event-enum";

@Component({
    selector: 'app-transaction-table',
    templateUrl: './transaction-table.component.html',
    styleUrls: ['./transaction-table.component.scss']
})
export class TransactionTableComponent implements OnInit {

    @Input()
    walletId: string;

    @Input()
    transactions: TransactionTransactionOutputDTO[];

    @Input()
    currency: string;

    selectedItem: TransactionTransactionOutputDTO;
    displayCreateDialog: boolean = false;

    constructor(
        private confirmationService: ConfirmationService,
        private messageService: MessageService,
        private transactionService: TransactionService,
        private eventService: EventService
    ) {
    }

    ngOnInit(): void {
    }

    rowSelected() {
        console.log("Selected", this.selectedItem)
    }

    deleteTransaction(selectedItem: TransactionTransactionOutputDTO) {
        this.confirmationService.confirm({
            message: 'Do you want to delete this record?',
            header: 'Delete Confirmation',
            icon: 'pi pi-info-circle',
            accept: () => {
                this.transactionService.deleteTransactionItem(selectedItem.id.toString())
                    .subscribe({
                        next: () => {
                            this.messageService.add({severity: 'success', summary: 'Transaction deleted'});
                            this.eventService.emitEvent({
                                type: EventEnum.TRANSACTION_DELETED,
                                value: selectedItem
                            });
                        }
                    });
            }
        });
    }
}
