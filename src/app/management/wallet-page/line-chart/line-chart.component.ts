import {Component, Input, OnInit} from '@angular/core';
import {TransactionTransactionOutputDTO} from "../../../codegen";
import {DatePipe} from "@angular/common";

@Component({
    selector: 'app-line-chart',
    templateUrl: './line-chart.component.html',
    styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnInit {

    @Input()
    transactions: TransactionTransactionOutputDTO[]

    lineData: any;
    lineOptions: any;

    constructor(
        private datePipe: DatePipe
    ) {
        this.lineOptions = {
            plugins: {
                legend: {
                    labels: {
                        fontColor: '#A0A7B5'
                    }
                }
            },
            scales: {
                x: {
                    ticks: {
                        color: '#A0A7B5'
                    },
                    grid: {
                        color: 'rgba(160, 167, 181, .3)',
                    }
                },
                y: {
                    ticks: {
                        color: '#A0A7B5'
                    },
                    grid: {
                        color: 'rgba(160, 167, 181, .3)',
                    }
                },
            }
        };
    }

    ngOnInit(): void {
        const reversedChart = [...this.transactions].reverse();

        this.lineData = {
            labels: reversedChart.map(m => m.dateTime)
                .map(m => this.datePipe.transform(m, 'yyyy-MM-dd HH:mm')),
            datasets: [
                {
                    label: 'Invested Sum',
                    data: reversedChart.map(m => m.totalFiatInvested),
                    fill: false,
                    backgroundColor: '#f9ae61',
                    borderColor: '#f9ae61',
                    tension: .4
                },
                {
                    label: 'Wallet Value',
                    data: reversedChart.map(m => m.fiatWalletValue),
                    fill: false,
                    backgroundColor: '#90cd93',
                    borderColor: '#90cd93',
                    tension: .4
                }
            ]
        };
    }

}
