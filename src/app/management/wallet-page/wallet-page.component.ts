import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Subject, takeUntil} from "rxjs";
import {TransactionTransactionOutputDTO, WalletService, WalletWalletOutputDTO} from "../../codegen";
import {EventService} from "../../shared/service/event.service";
import {filter} from "rxjs/operators";
import {EventEnum} from "../../shared/service/model/event-enum";

@Component({
    selector: 'app-wallet-page',
    templateUrl: './wallet-page.component.html',
    styleUrls: ['./wallet-page.component.scss']
})
export class WalletPageComponent implements OnInit, OnDestroy {

    protected destroy$ = new Subject<void>();

    walletId: number = null;
    walletCurrency: string;
    currentWallet: WalletWalletOutputDTO;

    transactions: TransactionTransactionOutputDTO[];

    constructor(
        private route: ActivatedRoute,
        private walletService: WalletService,
        private eventService: EventService
    ) {
        this.route.params
            .pipe(takeUntil(this.destroy$))
            .subscribe((params) => {
                this.walletId = params.walletId;
                this.loadWallet();
                this.loadTransactions();
            });

        this.eventService.event$
            .pipe(filter(event =>
                event.type === EventEnum.TRANSACTION_CREATED ||
                event.type === EventEnum.TRANSACTION_DELETED
            )).subscribe((event) => {
            if (event.type === EventEnum.TRANSACTION_CREATED) {
                this.transactions.push(event.value);
            } else if (event.type === EventEnum.TRANSACTION_DELETED) {
                this.transactions.splice(
                    this.transactions.indexOf(event.value), 1
                );
            }

            this.transactions = this.transactions.sort((t1, t2) =>
                Date.parse(t2.dateTime) - Date.parse(t1.dateTime)
            );
        });
    }

    loadWallet() {
        this.walletService.getWalletItem(this.walletId.toString())
            .subscribe((wallet) => {
                this.currentWallet = wallet;
                this.walletCurrency = wallet.fiatCurrency;
            })
    }

    loadTransactions() {
        this.transactions = null;
        this.walletService.apiWalletsTransactionsGetSubresourceWalletSubresource(this.walletId.toString())
            .subscribe((transactions) => {
                this.transactions = transactions;
            })
    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

}
