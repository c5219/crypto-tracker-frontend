import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {WalletService, WalletWalletOutputDTO} from "../../codegen";
import {ActivatedRoute, Router} from "@angular/router";
import {EventService} from "../../shared/service/event.service";
import {EventEnum} from "../../shared/service/model/event-enum";
import {Subject, takeUntil} from "rxjs";

@Component({
    selector: 'app-create-wallet',
    templateUrl: './create-wallet.component.html',
    styleUrls: ['./create-wallet.component.scss']
})
export class CreateWalletComponent implements OnInit, OnDestroy {

    protected destroy$ = new Subject<void>();

    walletInEdition: WalletWalletOutputDTO;

    form: FormGroup;

    constructor(
        private fb: FormBuilder,
        private walletService: WalletService,
        private router: Router,
        private route: ActivatedRoute,
        private eventService: EventService
    ) {
        this.route.params
            .pipe(takeUntil(this.destroy$))
            .subscribe((params) => {
                if (params.walletId) {
                    this.walletService.getWalletItem(params.walletId)
                        .subscribe(wallet => {
                            this.walletInEdition = wallet;

                            this.form.patchValue({
                                id: wallet.id,
                                name: wallet.name,
                                coin: wallet.coin,
                                fiatCurrency: wallet.fiatCurrency
                            })
                        });
                }
            });
    }

    ngOnInit(): void {
        this.form = this.fb.group({
            id: [],
            name: ['', Validators.required],
            coin: ['', Validators.required],
            fiatCurrency: ['EUR', Validators.required]
        });
    }

    submit() {
        const handler = (wallet) => {
            this.eventService.emitEvent({
                type: EventEnum.WALLET_CREATED,
                value: wallet
            });

            this.router.navigate(['/wallets', wallet.id]);
        };

        if (this.walletInEdition) {
            this.walletService.putWalletItem(
                this.form.value['id'],
                this.form.value
            ).subscribe(handler);
        } else {
            this.walletService.postWalletCollection({
                name: this.form.value['name'],
                coin: this.form.value['coin'],
                fiatCurrency: this.form.value['fiatCurrency']
            }).subscribe(handler);
        }
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }
}
