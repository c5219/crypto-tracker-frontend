import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {AppMainComponent} from './app.main.component';
import {NoAuthGuard} from "./shared/guard/noauth.guard";
import {LoginComponent} from "./login/login/login.component";
import {AuthGuard} from "./shared/guard/auth.guard";

@NgModule({
    imports: [
        RouterModule.forRoot([
            {
                path: 'login',
                component: LoginComponent,
                canActivate: [NoAuthGuard]
            },
            {
                path: '',
                component: AppMainComponent,
                children: [
                    {
                        path: '',
                        loadChildren: () => import('./management/management.module').then(m => m.ManagementModule),
                        canLoad: [AuthGuard],
                        canActivate: [AuthGuard]
                    },

                ]
            },
            {path: '**', redirectTo: 'pages/empty'},
        ], {scrollPositionRestoration: 'enabled'})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
