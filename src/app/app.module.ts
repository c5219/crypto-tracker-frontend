import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {LocationStrategy, HashLocationStrategy, DatePipe} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AppMainComponent} from './app.main.component';
import {SharedModule} from "./shared/shared.module";
import {ApiModule, BASE_PATH, Configuration, ConfigurationParameters} from "./codegen";
import {environment} from "../environments/environment";
import {HttpClientModule} from "@angular/common/http";
import {LoginComponent} from "./login/login/login.component";
import {ConfirmationService, MessageService} from "primeng/api";


export function apiClientConfigFactory(): Configuration {
    const params: ConfigurationParameters = {
        withCredentials: false,
        apiKeys: {
            "Bearer": ""
        }
    };
    return new Configuration(params);
}


@NgModule({
    imports: [
        BrowserModule,
        SharedModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        ApiModule.forRoot(apiClientConfigFactory)
    ],
    declarations: [
        AppComponent,
        AppMainComponent,
        LoginComponent
    ],
    providers: [
        {provide: LocationStrategy, useClass: HashLocationStrategy},
        {provide: BASE_PATH, useValue: environment.apiUrl},
        MessageService, ConfirmationService, DatePipe,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
